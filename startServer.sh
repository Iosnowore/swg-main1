#!/bin/bash
echo "*************************"
echo "Starting NGE Server v2.6"
echo "*************************"
cd $(pwd)/exe/linux

killall LoginServer &> /dev/null
killall CentralServer &> /dev/null
killall ChatServer &> /dev/null
killall CommoditiesServer &> /dev/null
killall ConnectionServer &> /dev/null
killall CustomerServiceServer &> /dev/null
killall LogServer &> /dev/null
killall MetricsServer &> /dev/null
killall PlanetServer &> /dev/null
killall ServerConsole &> /dev/null
killall SwgDatabaseServer &> /dev/null
killall SwgGameServer &> /dev/null 
killall TransferServer &> /dev/null

./bin/LoginServer -- @servercommon.cfg &

sleep 5

./bin/TaskManager -- @servercommon.cfg
