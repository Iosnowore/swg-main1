#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=
DBUSERNAME=
DBPASSWORD=
HOSTIP=
CLUSTERNAME=
NODEID=
DSRC_DIR=
DATA_DIR=
MODE=Release

if [ ! -d $basedir/build ]
then
	mkdir $basedir/build
fi

read -p "**********************************************************
This script will compile the src to binaries.
New bins will be located in /home/swg/swg-main1/build/bin
**********************************************************
Do you want to build the server now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
  
        unset ORACLE_HOME;
        unset ORACLE_SID;
        unset JAVA_HOME;   
        export ORACLE_HOME=/usr/lib/oracle/12.1/client;
        export JAVA_HOME=/usr/java;
        export ORACLE_SID=swg;
        rm -rf /home/swg/swg-main1/build
        mkdir /home/swg/swg-main1/build
	mkdir /home/swg/swg-main1/build/bin
	cd $basedir/build	

	if [ $(arch) == "x86_64" ]; then
        	export LDFLAGS=-L/usr/lib32
		export CMAKE_PREFIX_PATH="/usr/lib32:/lib32:/usr/lib/i386-linux-gnu:/usr/include/i386-linux-gnu"

		cmake -DCMAKE_C_FLAGS=-m32 \
		-DCMAKE_CXX_FLAGS=-m32 \
		-DCMAKE_EXE_LINKER_FLAGS=-m32 \
		-DCMAKE_MODULE_LINKER_FLAGS=-m32 \
		-DCMAKE_SHARED_LINKER_FLAGS=-m32 \
		-DCMAKE_BUILD_TYPE=$MODE \
		$basedir/src
	else
		cmake $basedir/src -DCMAKE_BUILD_TYPE=$MODE
	fi

	make -j$(nproc)
	cd $basedir
fi
echo "Congratulations the build_linux script is complete!"
